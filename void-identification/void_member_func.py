#void_member_func.py
import numpy as np


def void_member(halo_ID, Type, void_ID, halo_void_ID, dist_to_void):

    """
    Matches each galaxy with the void when corresponding.


    @param halo_ID Integer array with the IDs of the SAG catalog.
    @param Type Type of the galaxy of the SAG catalog.
    @param void_ID Array with the IDs of the voids.
    @param halo_void_ID Array with the halo ID that belong to a certain void.
    @param dist_to_void Distance from the halo to the void, in units of Rvoid.

    void_ID, halo_void_ID and dist_to_void are ordered according to halo_void_ID... 

    @return (gx_void_ID, halo_dist_to_void).
    """

    # Number of haloes
    Ngal = len(halo_ID)

    # Initializing saving arrays
    gx_void_ID = np.ones(Ngal)*(-2)
    halo_dist_to_void = np.ones(Ngal)*(-2)

    halo_void_ID_set = set(halo_void_ID)

    i = 0
    isat = 0

    try:
        while(i < Ngal-1):

            # If galaxy is type 0
            if(Type[i] == 0):

                if(len(set(halo_ID[i].flat).intersection(halo_void_ID_set))==1): # belongs to void?

                    ind = np.searchsorted(halo_void_ID,halo_ID[i],side='left')
                    gx_void_ID[i] = void_ID[ind]
                    halo_dist_to_void[i] = dist_to_void[ind]

                # if galaxy not in void
                else:
                    gx_void_ID[i] = -1
                    halo_dist_to_void[i] = -1

                isat = i+1

                temp_ID = gx_void_ID[i]
                temp_dist = halo_dist_to_void[i]

                while(Type[isat] != 0):

                    # If galaxy is type 1
                    if(Type[isat] == 1):

                        if(len(set(halo_ID[isat].flat).intersection(halo_void_ID_set))==1):

                            ind = np.searchsorted(halo_void_ID,halo_ID[isat],side='left')    
                            gx_void_ID[isat] = void_ID[ind]
                            halo_dist_to_void[isat] = dist_to_void[ind]
                            temp_ID = gx_void_ID[isat]

                        # if galaxy not in void
                        else:
                            gx_void_ID[isat] = -1
                            halo_dist_to_void[isat] = -1
                            temp_ID = -1
                            temp_dist = -1

                    # If galaxy is type 2
                    else:
                        gx_void_ID[isat] = temp_ID # orphans belongs to the same void as their parent
                        halo_dist_to_void[isat] = temp_dist

                    isat += 1



                i = isat

            else: # if first halo is not central, move to the next
                i += 1      # will not check if this type 1 or two belongs to void because every file starts
                            # with a type 0 galaxy.

    # Checking all galaxies are analysed
    except IndexError:
        pass

    return gx_void_ID, halo_dist_to_void

