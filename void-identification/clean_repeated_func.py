#clean_repeated_func.py
import numpy as np

def clean_repeated(halo_ID, void_ID, dist_to_void):

	"""
        Function to remove repeated IDs in the arrays, according to 
	    the criteria that the halo closer to a void remains.
	    First removes halos repeated 3 times (found manually)
	    Then orders the arrays and remove repeated 2 times.

        @param halo_id Id of the halo.
        @param void_id ID of the void.
        @param dist_to_void distance of halo to centre of void.

	@return (halo_ID_order, void_ID_order, dist_to_void_order)
            returns same properties, but order by halo and matching
            the halo with its closest void.
        """

	# Remove IDs repeated 3 times (manually)
	halo_ID = np.delete(halo_ID, [5635845,6604953,13603,1035043,6091479,11483001])
	void_ID = np.delete(void_ID, [5635845,6604953,13603,1035043,6091479,11483001])
	dist_to_void = np.delete(dist_to_void, [5635845,6604953,13603,1035043,6091479,11483001])

	
	order_ind = np.argsort(halo_ID)

	halo_ID_order = halo_ID[order_ind]
	void_ID_order = void_ID[order_ind]
	dist_to_void_order = dist_to_void[order_ind]


	mask_unique = np.ones_like(halo_ID, dtype=bool)

	for i in range(len(halo_ID)-1):

		if(halo_ID[i]==halo_ID[i+1]):

			if(dist_to_void[i]<dist_to_void[i+1]): # I keep the ID with lower distance to a void
				mask_unique[i+1] = False
			else:
				mask_unique[i] = False


	halo_ID_order = halo_ID_order[mask_unique]
	void_ID_order = void_ID_order[mask_unique]
	dist_to_void_order = dist_to_void_order[mask_unique]

	return halo_ID_order, void_ID_order, dist_to_void_order
    
