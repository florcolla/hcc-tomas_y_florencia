#void_ID_main.py
import numpy as np
import h5py
import SAGreader as sag
from void_member_func import void_member
from clean_repeated_func import clean_repeated
import time

# Setting initial time to print on screen
start_time = time.time()

# path of the simulation
sim_path = '/d3/Tom/MDPL/SalidaSAM/SAG-7.130-tomas_aF1.3_mag_compl/'
# name of the simulation (hdf5 file)
sim_name = 'gal_125_SAG-7.130-tomas_aF1.3_mag_BOX_'
# path of the file of the matched halo-void
voids_path = '/d3/Tom/MDPL/Voids/gxs_in_voids.dat'

print('Reading Void data')
void_ID, halo_void_ID = np.loadtxt(voids_path, dtype=int, usecols=[0,1], unpack=True)
dist_to_void = np.loadtxt(voids_path, usecols=[2], unpack=True)

# remove IDs that are repeated 3 times in the array (just 3)
print('')
print('Removing and cleaning the array')

halo_void_ID_order, void_ID_order, dist_to_void_order = clean_repeated(halo_void_ID, void_ID, dist_to_void)

# use SAGreader routine to load DM haloes data stored in hdf5 files

print('Reading Sim data')
sagdat = sag.SAGdata('MDPL', 1000./128., keepOpen=False)

Nbox = 128

for ibox in range(1,Nbox+1):

    sagdat.addFile(sim_path+sim_name+str(ibox).zfill(3)+'.hdf5')

    Halo_sim_ID = sagdat.readDataset('HaloID')
    Type = sagdat.readDataset('Galaxy_Type')

    sagdat.clear()

    Ngal = len(Type)

    # get the ID of void for every galaxy, and the distance to the void
    print
    print('Matching galaxy and halo ID with clean void catalog for BOX '+str(ibox).zfill(3))
    print('Processing '+str(Ngal)+' galaxies')

    # Calling function void_member
    gx_void_ID, gx_dist_to_void = void_member(Halo_sim_ID, Type, void_ID_order, halo_void_ID_order, dist_to_void_order)

    print('Creating hdf5 file : ' + 'GalVoidMatch/BOX_'+str(ibox).zfill(3)+'_Void_ID.hdf5')

    f = h5py.File(sim_path+'GalVoidMatch/BOX_'+str(ibox).zfill(3)+'_Void_ID.hdf5', 'w')

    f.create_dataset('void_id', data=gx_void_ID).attrs['Description'] = 'ID of the void where the galaxy is in. -1 if its not in a void'
    f.create_dataset('dist_to_void', data=gx_dist_to_void).attrs['Description'] = 'Distance to the center of the void, normalized to Rvoid'

    f.close()


print ( "--- tiempo en minutos ---", (time.time() - start_time)/60.0)

print('Done!')
