#plot_functions.py
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl


def set_style(style='book', Hratio=1.0, Wfrac=1.0):

    """
    Function to change style properties of plots in matplotlib
    such as sizes of figure, font type and size, frame of legend, etc. 

    @param style str that define which style the plot will 
            have (talk, book, mnras, mnras-fw)
    @param Hratio ratio between sides of the final plot (all of the plot).
            It defines if the plot will be square or rectangular and how.
    @param Wfrac parameter that makes bigger or smaller the plot.
            Same for both sides.
    """


    if style == 'talk':
        size, fsize = 6, 16
    if style == 'book':
        size, fsize = 5.39, 12
    if style == 'mnras':
        size, fsize = 3.32, 8.1
    if style == 'mnras-fw':
        size, fsize = 6.97, 8

    mpl.rcParams['figure.figsize'] = (size*Wfrac, (size*Wfrac)*Hratio)
    mpl.rcParams['font.family'] = 'serif'
    mpl.rcParams['font.serif'] = ['Times', 'Liberation Serif', 'Times New Roman']
    mpl.rcParams['font.size'] = fsize
    mpl.rcParams['legend.fontsize'] = 'medium'
    mpl.rcParams['legend.frameon'] = True
    mpl.rcParams['text.usetex'] = True
    mpl.rcParams['legend.edgecolor'] = 'k'
    mpl.rcParams['axes.linewidth'] = 1.0
    try:
        mpl.rcParams['xtick.minor.visible'] = True
        mpl.rcParams['ytick.minor.visible'] = True
    except: pass
    try:
        mpl.rcParams['xtick.top'] = True
        mpl.rcParams['ytick.right'] = True
    except: pass
    try:
        mpl.rcParams['xtick.direction'] = 'in'
        mpl.rcParams['ytick.direction'] = 'in'
    except: pass



def xy_distribution(x,y, void_ID, Nfil):

    """
    XY distribution of all galaxies, and separated in voids and filaments. 

    @param x position x of galaxies.
    @param y position y of galaxies.
    @param void_ID ID of the void in which the galaxy resides. 
            If -1 the galaxy is not in a void.
    @param Nfil number of filaments close to the galaxy.
    """

    # Mask for galaxies in voids
    mask_void = void_ID > 0
    x_void, y_void = x[mask_void], y[mask_void]

    # Mask for galaxies in filaments
    mask_fil = Nfil > 0 
    x_fil, y_fil = x[mask_fil], y[mask_fil]
        
    ########## PLOT: xy spatial distribution of all galaxies ############
    set_style('mnras')

    fig, ax = plt.subplots(1,1)

    # Setting labels and limits
    ax.set_xlabel('x [Mpc]')
    ax.set_ylabel('y [Mpc]')

    ax.set_xlim([500,1000])
    ax.set_ylim([500,1000])

    # For he plot to be square
    plt.gca().set_aspect('equal', adjustable='box')

    ax.plot(x,y, 'o', color='olivedrab', ms=0.08)

    plt.tight_layout()

    # Savinbg plot
    plt.savefig('All.png', dpi=400)

    plt.clf()

    print('Plot of all galaxies done')


    ########## PLOT: xy spatial distribution of galaxies in voids and galaxies in filaments ############
    set_style('mnras')

    fig, ax = plt.subplots(1,1)

    # Setting lables and limits
    ax.set_xlabel('x [Mpc]')
    ax.set_ylabel('y [Mpc]')

    ax.set_xlim([500,1000])
    ax.set_ylim([500,1000])

    # For he plot to be square
    plt.gca().set_aspect('equal', adjustable='box')

    ax.plot(x_void,y_void, 'o', color='skyblue', ms=0.8, label='In voids')
    ax.plot(x_fil,y_fil, 'o', color='tomato', ms=0.08, label='In filaments')

    # Printing a legend
    legend = ax.legend(loc='upper right', numpoints=1, fontsize=6)
    # changing the sizes of the symbols in legend
    legend.legendHandles[0]._legmarker.set_markersize(3)
    legend.legendHandles[1]._legmarker.set_markersize(3)

    plt.tight_layout()

    # Saving plot
    plt.savefig('V-F.png', dpi=400)

    plt.clf()

#    print('Plot of galaxies in voids and filaments done')

    return 0


# --------------------------------------------------------------------------------- #


def f_passive(Mstar, sfr, void_ID, Nfil):

    """
    Fraction of passive galaxies in voids, in filaments and for all galaxies.

    @param Mstar stellar mass of galaxies.
    @param sfr star formation rate of galaxies.
    @param void_ID ID of the void in which the galaxy resides. 
            -1 if the galaxy is not in a void.
    @param Nfil number of filaments close to the galaxy.
    """

    # Mask for galaxies in voids
    mask_void = void_ID > 0
    Mstar_void, sfr_void = Mstar[mask_void], sfr[mask_void]

    # Mask for galaxies in filaments
    mask_fil = Nfil > 0 
    Mstar_fil, sfr_fil = Mstar[mask_fil], sfr[mask_fil]

    ssfr, ssfr_void, ssfr_fil = sfr/Mstar, sfr_void/Mstar_void, sfr_fil/Mstar_fil

    mask_p = np.log10(ssfr)<-10.7
    mask_p = mask_p.reshape(len(ssfr))

    mlow, mhigh = np.min(np.log10(Mstar)), np.max(np.log10(Mstar))

    hist_all, bin_edges = np.histogram(np.log10(Mstar), bins=12, range=(mlow,mhigh))

    hist_all_p, _ = np.histogram(np.log10(Mstar[mask_p]), bins=12, range=(mlow,mhigh))

    hist_void, bin_edges = np.histogram(np.log10(Mstar[mask_void]), bins=12, range=(mlow,mhigh))
    hist_void_p, _ = np.histogram(np.log10(Mstar[mask_void&mask_p]), bins=12, range=(mlow,mhigh))

    hist_fil, bin_edges = np.histogram(np.log10(Mstar[mask_fil]), bins=12, range=(mlow,mhigh))
    hist_fil_p, _ = np.histogram(np.log10(Mstar[mask_fil&mask_p]), bins=12, range=(mlow,mhigh))

    fp_all = hist_all_p.astype(float)/hist_all

    fp_void = hist_void_p.astype(float)/hist_void

    fp_fil = hist_fil_p.astype(float)/hist_fil

    bin_x = (bin_edges[1:]+bin_edges[:-1])/2.0


    ## PLOT: fraction of passive galaxies as a function of stellar mass ## 
    set_style('mnras')

    fig, ax = plt.subplots(1,1)

    # Setting lables and limits
    ax.set_xlabel(r'log($M_\star [\rm{M_\odot}]$)')
    ax.set_ylabel('Passive fraction')

    ax.set_xlim([8,12.5])
    ax.set_ylim([0,1.1])

    ax.plot(bin_x, fp_all, '-', color='olivedrab', lw=2, label='All galaxies')
    ax.plot(bin_x, fp_void, '--', color='skyblue', lw=2, label='In voids')
    ax.plot(bin_x, fp_fil, ':', color='tomato', lw=4, label='In filaments')

    plt.legend(loc='upper left', numpoints=1, fontsize=6)

    plt.tight_layout()

    plt.savefig('Passive_fraction.png', dpi=400)

    plt.clf()

    print('Plot of passive fraction done')

    return 0


# --------------------------------------------------------------------------------- #


def Cal_Median(X_input, Y_input, Dimension):

    """
    Function that calculates de median of a distribution.

    @param X_input independent property to calculate de median.
    @param Y_input dependent property to calculate de median.
    @param Dimension dimension of return values (also the amount 
        of bins in which the distribution will be cut).

    @return (x_median, y_median, y_percentile84, y_percentile16, NumGal_inBin) 
        Arrays of shape (Dimension,) which represent the x and y variables, 
        the 84th and 16th percentiles and the number of elements in each bin.
    """

    #     ARRAYS TO RETURN
    x_median       = np.zeros(Dimension)
    y_median       = np.zeros(Dimension)
    y_percentile84 = np.zeros(Dimension)
    y_percentile16 = np.zeros(Dimension)
    NumGal_inBin   = np.zeros((Dimension,), dtype=int)
    
    #     LIMITS IN CALCULATION
    Min_value = min(X_input)
    Max_value = max(X_input)
    Delta     = ( Max_value - Min_value ) / float(Dimension)
    
    for ibin in range(Dimension):
        inf  = Min_value + ibin * Delta
        sup  = Min_value + (ibin+1) * Delta
        mask = (X_input >= inf) & (X_input < sup)
        NumGal_inBin[ibin] = len(mask[mask])
        del inf
        del sup
        
        if (NumGal_inBin[ibin] > 0):
            # MEDIAN
            x_median[ibin]       = np.median(X_input[mask])
            y_median[ibin]       = np.median(Y_input[mask])

            # PERCENTILE
            y_percentile84[ibin] = np.percentile(Y_input[mask], 84)
            y_percentile16[ibin] = np.percentile(Y_input[mask], 16)

        del mask
    del ibin

    return x_median, y_median, y_percentile84, y_percentile16, NumGal_inBin


# ----------------------------------------------------------------------------------------------------- #


def gas_fraction(Mstar, Mgas, void_ID, Nfil):

    """    Function that plots gas mass vs. stellar mass.

    @param Mstar stellar mass of galaxies.
    @param Mgas cold gas mass of galaxies.
    @param void_ID ID of the void in which the galaxy resides. 
            -1 if the galaxy is not in a void.
    @param Nfil number of filaments close to the galaxy.
    """

    # Generate gas fraction as f_gas = Mgas / (Mgas + Mstar)
    f_gas = np.divide(Mgas, Mgas + Mstar)

    # Mask for galaxies in voids
    mask_void = void_ID > 0
    Mstar_void, f_gas_void = np.log10(Mstar[mask_void]), f_gas[mask_void]

    # Mask for galaxies in filaments
    mask_fil = Nfil > 0
    Mstar_fil, f_gas_fil = np.log10(Mstar[mask_fil]), f_gas[mask_fil]

    #### PLOT: gas fraction vs. stellar mass ###
    set_style('mnras')

    fig, ax = plt.subplots(1,1)

    # Setting labels and limits
    ax.set_xlabel(r'$\log(M_\star \, {\rm [M_\odot]})$')
    ax.set_ylabel(r'$f_{\rm gas} = M_{\rm gas} / (M_{\rm gas} + M_\star) $')
    ax.set_xlim([8,12])
    ax.set_ylim([0,0.8])

    #plt.gca().set_aspect('equal', adjustable='box')

    # Plotting all
    Xmedian, Ymedian, Yp84, Yp16, NumMedian = Cal_Median(np.log10(Mstar), f_gas, 10)
    ax.plot(Xmedian[NumMedian>=10], Ymedian[NumMedian>=10], '-', color='olivedrab', lw=2, label='All')

    # Plotting in viods
    Xmedian, Ymedian, Yp84, Yp16, NumMedian = Cal_Median(Mstar_void, f_gas_void, 10)
    ax.plot(Xmedian[NumMedian>=10], Ymedian[NumMedian>=10], '--', color='skyblue', lw=2, label='In voids')

    # Plotting in filaments
    Xmedian, Ymedian, Yp84, Yp16, NumMedian = Cal_Median(Mstar_fil, f_gas_fil, 10)
    ax.plot(Xmedian[NumMedian>=10], Ymedian[NumMedian>=10], ':', color='tomato', lw=4, label='In filaments')

    # Generate legend of plot
    legend = ax.legend(loc='upper right', numpoints=1, fontsize=6, handlelength=2.5)

    plt.tight_layout()

    # Saving plot
    plt.savefig('GasFraction.png', dpi=400)
    
    plt.clf()

    return 0


# ----------------------------------------------------------------------------------------------------- #


def main_sequence(Mstar, sfr, void_ID, Nfil):

    """
    Function that plots star formation rate vs. stellar mass.

    @param Mstar stellar mass of galaxies.
    @param sfr star formation rate of galaxies.
    @param void_ID ID of the void in which the galaxy resides. 
            -1 if the galaxy is not in a void.
    @param Nfil number of filaments close to the galaxy.
    """

    # Need galaxies with sfr>0
    mask = sfr > 0
    mask = mask.reshape(len(mask))
    Mstar   = Mstar[mask]
    sfr     = sfr[mask]
    void_ID = void_ID[mask]
    Nfil    = Nfil[mask]
    del mask

    # Keeping galaxies with active star formation define as in Brown et al. (2017)
    sSFR = np.divide(Mstar, sfr)
    mask = sSFR > 10**(-10.7)
    mask = mask.reshape(len(mask))
    Mstar   = np.log10(Mstar[mask])
    sfr     = np.log10(sfr[mask])
    void_ID = void_ID[mask]
    Nfil    = Nfil[mask]
    del mask, sSFR

    # Mask for galaxies in voids
    mask_void = void_ID > 0
    Mstar_void, sfr_void = Mstar[mask_void], sfr[mask_void]

    # Mask for galaxies in filaments
    mask_fil = Nfil > 0
    Mstar_fil, sfr_fil = Mstar[mask_fil], sfr[mask_fil]

    #### PLOT: star formation rate vs. stellar mass ###
    set_style('mnras')

    fig, ax = plt.subplots(1,1)

    # Setting labels and limits
    ax.set_xlabel(r'$\log(M_\star \, {\rm [M_\odot]})$')
    ax.set_ylabel(r'$\log({\rm SFR} \, {\rm [M_\odot \, yr^{-1}]})$')
    ax.set_xlim([8,12])
    ax.set_ylim([-2,1])

    # Plotting all
    Xmedian, Ymedian, Yp84, Yp16, NumMedian = Cal_Median(Mstar, sfr, 10)
    ax.plot(Xmedian[NumMedian>=10], Ymedian[NumMedian>=10], '-', color='olivedrab', lw=2, label='All')

    # Plotting in viods
    Xmedian, Ymedian, Yp84, Yp16, NumMedian = Cal_Median(Mstar_void, sfr_void, 10)
    ax.plot(Xmedian[NumMedian>=10], Ymedian[NumMedian>=10], '--', color='skyblue', lw=2, label='In voids')

    # Plotting in filaments
    Xmedian, Ymedian, Yp84, Yp16, NumMedian = Cal_Median(Mstar_fil, sfr_fil, 10)
    ax.plot(Xmedian[NumMedian>=10], Ymedian[NumMedian>=10], ':', color='tomato', lw=4, label='In filaments')

    # Generating legend of plot
    legend = ax.legend(loc='lower right', numpoints=1, fontsize=6, handlelength=2.5)

    plt.tight_layout()

    # Saving plot
    plt.savefig('MainSequence.png', dpi=400)

    plt.clf()

    return 0
