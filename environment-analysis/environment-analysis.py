#environment-analysis.py
import numpy as np
import matplotlib.pyplot as plt
import h5py
import SAGreader as sag
from plot_functions import *

# path where the simulation is
path_sim = '/d3/Tom/MDPL/SalidaSAM/SAG-7.130-tomas_aF1.3_mag_compl/'
# name of the hdf5 files
simname = 'gal_125_SAG-7.130-tomas_aF1.3_mag_BOX_'
# path of the void processed files
path_voids = path_sim+'GalVoidMatch/'
# path of the filament processed files
path_fil = path_sim+'GalFilMatch/'

# Number of boxes to analyse (max 128)
Nboxes = 10

# Lower stellar mass limit (depends on DMO simulation resolution)
masslim = 1e8

# Calling SAGreader to upload galaxy properties
sagdat = sag.SAGdata('MDPL', 1000., keepOpen=False)

for box in range(1,Nboxes+1):

	sagdat.addFile(path_sim+simname+str(box).zfill(3)+'.hdf5')

# reading default units
un = sagdat.readUnits()

# Reading positions in units of Mpc
x = sagdat.readDataset('X') / un.h / 1000.
y = sagdat.readDataset('Y') / un.h / 1000.
z = sagdat.readDataset('Z') / un.h / 1000.

# Reading Stellar Mass in units of Msun
Mstar = sagdat.readDataset('M_star_bulge') / un.h + sagdat.readDataset('M_star_disk') / un.h

# Applying mask of Stellar Mass limit (masslim)
mask = Mstar>=masslim
mask = mask.reshape(len(Mstar))

Mstar = Mstar[mask]

# Reading star formation rate in units of Msun / yr
sfr = sagdat.readDataset('SFR') / un.h

# Reading Gas Mass in units of Msun
Mgas = sagdat.readDataset('M_gas_bulge') / un.h + sagdat.readDataset('M_gas_disk') / un.h

# Applying mask of Stellar Mass limit
Mgas = Mgas[mask]
sfr = sfr[mask]
x, y, z = x[mask], y[mask], z[mask]

# Reading the void files (also using SAGreader)
voiddat = sag.SAGdata('VOIDS', 1000., keepOpen=False)

for box in range(1,Nboxes+1):
	voiddat.addFile(path_voids+'BOX_'+str(box).zfill(3)+'_Void_ID.hdf5')

# Reading void ID (if -1 galaxy not attached to void)
void_ID = voiddat.readDataset('void_id')

# Applying mask of Stellar Mass limit
void_ID = void_ID[mask]

# Reading the filament files (also using SAGreader)
fildat = sag.SAGdata('FILAMENT', 1000., keepOpen=False)

for box in range(1,Nboxes+1):
	fildat.addFile(path_fil+'BOX_'+str(box).zfill(3)+'_Fil_Prop.hdf5')

# Reading Number of filaments that each galaxy originally is in
Nfil = fildat.readDataset('Nfil')

# Applying mask of Stellar Mass limit
Nfil = Nfil[mask]

# To visualize large scale structure, we consider a slice of the simulation
# Only for plot XY distribution
maskz = ((z>150)&(z<200)).reshape(len(x))

print('Plots of XY distributions')
xy_distribution(x[maskz], y[maskz], void_ID[maskz], Nfil[maskz])

print('Plot of passive fraction')
f_passive(Mstar, sfr, void_ID, Nfil)

print('Plot of gas fraction')
gas_fraction(Mstar, Mgas, void_ID, Nfil)

print('Plot of star-forming main sequence')
main_sequence(Mstar, sfr, void_ID, Nfil)


print('Te congratulo, todos los plots salieron por un tubo')

