#select_filaments.py
import numpy as np
import h5py
import time

# Setting initial time to print on screen
start_time = time.time()

# path of the filament information
dir = '/d3/Tom/MDPL/Filamentos/'

# name of the file with filament information
name_seg = 'seg_close_to_halos.bin'
# name of the file with the ID of haloes
name_halos = 'halos_ID.dat'

# Convertor of units from Kpc to Mpc
POSFACTOR = 1000. 

N_halos = 127388161 - 1    # number of lines of sussing_125.z0.000.AHF_halos - header

# Initializing arrays
Id_filaments = np.ones(N_halos).astype(int)*(-1)
N_filaments = np.ones(N_halos).astype(int)*(-1)
r_par = np.ones(N_halos)*(-1)
r_per = np.ones(N_halos)*(-1)


print('Reading properties of filaments')


#################################################################################

# Setting the form in which the information is going to be store from the binary file
perttype_seg = np.dtype([
  ("Id",         np.uint32),
  ("rpar",       np.float32),
  ("rper",       np.float32),
  ])

def read_pert_seg(Path_pert,Name_pert,POSFACTOR):

    """
    Function that reads the binary file and transform it to arrays.

    @param Path_pert path where the file is located.
    @param Name_pert name of the files of filaments.
    @param POSFACTOR convertor from Kpc to Mpc.

    @return (pert) array of type perttype_seg.
    """

    filename = "%s%s" % (Path_pert,Name_pert)
    print "Reading file: %s" % (filename)

    # Opening binary file
    binario = open(filename,"rb")
    # Reading number of haloes and number of filaments
    N = np.fromfile(binario, dtype=np.int32, count=1)[0]
    Nsum = np.fromfile(binario, dtype=np.int32, count=1)[0]

    print "%d Halos " % (N)

    contador = 0
    # Initializing return element
    pert = []
    for i in xrange(N):
        chain = np.fromfile(binario, dtype=np.int32, count=1)[0]
        prop_pert = np.fromfile(binario, dtype=perttype_seg, count=chain).ravel()
        tmp = np.fromfile(binario, dtype=np.int32, count=1)[0]
        # checks that is reading ok
        assert(tmp==chain)
        contador += chain
        # if no filament contains that halo
        if(chain==0):
            pert.append(np.asarray([],dtype=perttype_seg))
        # if at least one match halo-filament
        else:
            prop_pert["rpar"] /= POSFACTOR
            prop_pert["rper"] /= POSFACTOR;
            pert.append(prop_pert)
        # prints on screen only if multiple of 1000000
        if(i%1000000==0): 
            print "%d %d %.2f" % (i,N,1.0*i/N)

    # another check
    assert(contador==Nsum)

    # transpose
    pert = np.asarray(pert)
    return pert

#################################################################################


# Calling function read_pert_seg
filaments_per_halo = read_pert_seg(dir, name_seg, POSFACTOR)

for ihalo in range(N_halos):

	N_filaments[ihalo] = len(filaments_per_halo[ihalo]["Id"])

        # If halo matched with more than 1 filament
	if(N_filaments[ihalo] > 1):

		ind = np.argmin(filaments_per_halo[ihalo]["rper"])

		Id_filaments[ihalo] = filaments_per_halo[ihalo]["Id"][ind]
		r_per[ihalo] = filaments_per_halo[ihalo]["rper"][ind]
		r_par[ihalo] = filaments_per_halo[ihalo]["rpar"][ind]

        # If halo matched with only 1 filament
	elif(N_filaments[ihalo] == 1):

		Id_filaments[ihalo] = filaments_per_halo[ihalo]["Id"]
		r_per[ihalo] = filaments_per_halo[ihalo]["rper"]
		r_par[ihalo] = filaments_per_halo[ihalo]["rpar"]

print ("--- tiempo en minutos  ---", (time.time() - start_time)/60.0)

# Due to space, this file only saves halos matched with at least 1 filament
mask = N_filaments>0

N_filaments = N_filaments[mask]
Id_filaments = Id_filaments[mask]
r_per = r_per[mask]
r_par = r_par[mask]

# Setting time again to check time in reading haloes ID
start_time = time.time()

print('Reading halos IDs')
halos_ID = np.loadtxt(dir+name_halos, unpack=True)

halos_ID = halos_ID[mask]

# Printing time that took reading halo ID
print ("--- tiempo en minutos  ---", (time.time() - start_time)/60.0)

f = h5py.File('closest_filament.hdf5', 'w')

f.create_dataset('HaloID', data=halos_ID)
f.create_dataset('FilamentID', data=Id_filaments)
f.create_dataset('Nfilaments', data=N_filaments)
f.create_dataset('rpar', data=r_par)
f.create_dataset('rper', data=r_per)

f.close()

# Printing final time from reading halo ID
print ("--- tiempo en minutos  ---", (time.time() - start_time)/60.0)

