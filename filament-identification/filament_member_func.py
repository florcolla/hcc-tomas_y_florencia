#filament_member_func.py
import numpy as np

def filament_member(halo_ID, Type, halo_fil_ID, Nfil, filID, rpar, rper):

    ''' 
    Matches each galaxy with a filament. If the galaxy is not in a filament,
    completes with -1.

    @param halo_ID Integer array with the IDs of the SAG catalog.
    @param Type Type of the galaxy of the SAG catalog.
    @param halo_fil_ID Array with the ID of the halos close to filaments.
    @param Nfil Number of filaments closer than 2 Mpc to a each halo.
    @param filID ID of the closest filament.
    @param rpar Distance to the most massive node.
    @param rper Perpendicular distance to the filament.

    
    @return (gx_Nfil, gx_filID, gx_rper, gx_rpar) Arrays of same shape 
        as halo_ID, which represent number of origial filament of that galaxy,
        Id of filament, rper of that filament, rpar of that filament.
        
    '''
    # number of galaxies
    Ngal = len(halo_ID)

    # generating arrays to save properties
    gx_Nfil = np.ones(Ngal)*(-2)
    gx_filID = np.ones(Ngal)*(-2)
    gx_rper = np.ones(Ngal)*(-2)
    gx_rpar = np.ones(Ngal)*(-2)

    # generating the sets
    halo_fil_ID_set = set(halo_fil_ID)

    i = 0
    isat = 0

    try:
        while(i < Ngal):

            # If the galaxy is Type 0
            if(Type[i] == 0):

                if(len(set(halo_ID[i].flat).intersection(halo_fil_ID_set))==1): # belongs to filament?

                    ind = np.searchsorted(halo_fil_ID,halo_ID[i],side='left')

                    gx_Nfil[i] = Nfil[ind]
                    gx_filID[i] = filID[ind]
                    gx_rper[i] = rper[ind]
                    gx_rpar[i] = rpar[ind]
                    
                # Galaxy is not in filament
                else:
                    gx_Nfil[i] = -1
                    gx_filID[i] = -1
                    gx_rper[i] = -1
                    gx_rpar[i] = -1
                    

                # going to next galaxy
                isat = i+1

                # saving filament values as to not find them again
                temp_Nfil = gx_Nfil[i]
                temp_filID = gx_filID[i]
                temp_rper = gx_rper[i]
                temp_rpar = gx_rpar[i]


                while(Type[isat] != 0):

                    # If galaxy is Type 1
                    if(Type[isat] == 1):

                        if(len(set(halo_ID[isat].flat).intersection(halo_fil_ID_set))==1):

                            ind = np.searchsorted(halo_fil_ID,halo_ID[isat],side='left') 

                            gx_filID[isat] = filID[ind]
                            gx_Nfil[isat] = Nfil[ind]
                            gx_rper[isat] = rper[ind]
                            gx_rpar[isat] = rpar[ind]

                            # saving filament values as to not find them again
                            temp_filID = gx_filID[isat]
                            temp_Nfil = gx_Nfil[isat]
                            temp_rper = gx_rper[isat]
                            temp_rpar = gx_rpar[isat]


                        # Galaxy is not in filament
                        else:
                            gx_filID[isat] = -1
                            gx_Nfil[isat] = -1
                            gx_rper[isat] = -1
                            gx_rpar[isat] = -1

                            
                            temp_filID = -1
                            temp_Nfil = -1
                            temp_rper = -1
                            temp_rpar = -1

                    # If galaxy is Type 2
                    else:

                        gx_filID[isat] = temp_filID  # orphans belongs to the same filament as their parent
                        gx_Nfil[isat] = temp_Nfil
                        gx_rper[isat] = temp_rper
                        gx_rpar[isat] = temp_rpar
 

                    isat += 1


                # Going to next central
                i = isat

            else: # if first galaxy is not central, move to the next
                i += 1      # will not check if this type 1 or 2 belongs to filament because every file starts
                            # with a type 0 galaxy.
    # Chequing that there is no -2
    except IndexError:
        pass

    return gx_Nfil, gx_filID, gx_rper, gx_rpar

