#gx_to_filament_main.py
import numpy as np
import h5py
import SAGreader as sag
from filament_member_func import filament_member
import time

# Setting initial time to print on screen
start_time = time.time()

# path of the simulation
sim_path = '/d3/Tom/MDPL/SalidaSAM/SAG-7.130-tomas_aF1.3_mag_compl/'
# name of the simulation (hdf5 file)
sim_name = 'gal_125_SAG-7.130-tomas_aF1.3_mag_BOX_'
# hdf5 file name of the matched halo-filament
fil_name = 'closest_filament.hdf5'

# Read Filament data, and sort arrays
f = h5py.File(fil_name, 'r')

halo_fil_ID = f['HaloID'].value
Nfil = f['Nfilaments'].value
fil_ID = f['FilamentID'].value
rpar = f['rpar'].value
rper = f['rper'].value

f.close()

# Sort arrays to search faster
sort_index = np.argsort(halo_fil_ID)
halo_fil_ID = halo_fil_ID[sort_index]
Nfil = Nfil[sort_index]
fil_ID = fil_ID[sort_index]
rpar = rpar[sort_index]
rper = rper[sort_index]


# use SAGreader routine to load DM haloes data stored in hdf5 files
print('Reading Sim data')
sagdat = sag.SAGdata('MDPL', 1000./128., keepOpen=False)

Nbox = 128

for ibox in range(1,Nbox+1):

    sagdat.addFile(sim_path+sim_name+str(ibox).zfill(3)+'.hdf5')

    Halo_sim_ID = sagdat.readDataset('HaloID')
    Type = sagdat.readDataset('Galaxy_Type')

    sagdat.clear()

    Ngal = len(Type)

    print
    print('Matching galaxy and halo ID with filament catalog for BOX '+str(ibox).zfill(3))
    print('Processing '+str(Ngal)+' galaxies')

    # Calling funtion filament_member
    gx_Nfil, gx_filID, gx_rper, gx_rpar = filament_member(Halo_sim_ID, Type, halo_fil_ID, Nfil, fil_ID, rpar, rper)

    print('Creating hdf5 file : ' + 'GalFilMatch/BOX_'+str(ibox).zfill(3)+'_Fil_Prop.hdf5')


    f = h5py.File(sim_path+'GalFilMatch/BOX_'+str(ibox).zfill(3)+'_Fil_Prop.hdf5', 'w')

    f.create_dataset('Nfil', data=gx_Nfil).attrs['Description'] = 'Number of filaments associated to the halo'
    f.create_dataset('fil_ID', data=gx_filID).attrs['Description'] = 'ID of closest filament to the halo'
    f.create_dataset('rper', data=gx_rper).attrs['Description'] = 'Perpendicular distance to the closest filament'
    f.create_dataset('rpar', data=gx_rpar).attrs['Description'] = 'Distance to the most massive node of the filament'

    f.close()

# Printing time on screen
print ( "--- tiempo en minutos ---", (time.time() - start_time)/60.0)

print('Done!')

